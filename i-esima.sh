#!/bin/bash

linha() {
    if [ $# -lt 2 ]; then
        echo "Uso: linha <número_da_linha> <arquivo>"
        return 1
    fi

    linha=$1
    arquivo=$2

    if [ -f "$arquivo" ]; then
        resultado=$(sed -n "${linha}p" "$arquivo")
        echo "$resultado"
    else
        echo "Arquivo não encontrado: $arquivo"
    fi
}

# Função para exibir a i-ésima coluna de um arquivo
coluna() {
    if [ $# -lt 2 ]; then
        echo "Uso: coluna <número_da_coluna> <arquivo>"
        return 1
    fi

    coluna=$1
    arquivo=$2

    if [ -f "$arquivo" ]; then
        resultado=$(awk -F: "{print \$$coluna}" "$arquivo")
        echo "$resultado"
    else
        echo "Arquivo não encontrado: $arquivo"
    fi
}
