#!/bin/bash

# Script principal

# Função para exibir a i-ésima linha de um arquivo
linha() {
    if [ $# -lt 2 ]; then
        echo "Uso: linha <número_da_linha> <arquivo>"
        return 1
    fi

    linha=$1
    arquivo=$2

    if [ -f "$arquivo" ]; then
        resultado=$(sed -n "${linha}p" "$arquivo")
        echo "$resultado"
    else
        echo "Arquivo não encontrado: $arquivo"
    fi
}

# Função para exibir a i-ésima coluna de um arquivo
coluna() {
    if [ $# -lt 2 ]; then
        echo "Uso: coluna <número_da_coluna> <arquivo>"
        return 1
    fi

    coluna=$1
    arquivo=$2

    if [ -f "$arquivo" ]; then
        resultado=$(awk -F: "{print \$$coluna}" "$arquivo")
        echo "$resultado"
    else
        echo "Arquivo não encontrado: $arquivo"
    fi
}

# Solicitar ao usuário o nome do arquivo
read -p "Digite o nome do arquivo: " arquivo

# Verificar se o arquivo existe
if [ ! -f "$arquivo" ]; then
    echo "Arquivo não encontrado: $arquivo"
    exit 1
fi

# Solicitar ao usuário escolher entre linhas ou colunas
read -p "Escolha entre 'linhas' ou 'colunas': " escolha

# Verificar a escolha do usuário
case "$escolha" in
    linhas)
        # Exibir o arquivo linha por linha
        echo "Exibindo o arquivo $arquivo linha por linha:"
        for ((i=1; i<=7; i++)); do
            linha $i "$arquivo"
        done
        ;;
    colunas)
        # Exibir o arquivo coluna por coluna
        echo "Exibindo o arquivo $arquivo coluna por coluna:"
        for ((i=1; i<=7; i++)); do
            coluna $i "$arquivo"
        done
        ;;
    *)
        echo "Escolha inválida. Por favor, escolha entre 'linhas' ou 'colunas'."
        exit 1
        ;;
esac
